<?php
$pageid = '299181444242';
//$pageid = 'rtpmadeira';

$img_json = file_get_contents('http://graph.facebook.com/' . $pageid . '/?fields=picture&type=large');
$img_obj = json_decode($img_json);
$json = file_get_contents('https://graph.facebook.com/' . $pageid . '/posts?access_token=827835443977109|ba6e9619919ad1f692e66f219eae368d&limit=3&fields=likes.limit(0).summary(true),comments.limit(0).summary(true),message,shares,updated_time,id');
$obj = json_decode($json);

$xml_info = new SimpleXMLElement("<?xml version=\"1.0\"?><fb_info></fb_info>");

$imagestuff = $xml_info->addChild('image');
if(property_exists($img_obj, 'picture'))
	if(property_exists($img_obj->picture, 'data'))
		if(property_exists($img_obj->picture->data, 'url'))
			$imagestuff->addChild('url', htmlspecialchars($img_obj->picture->data->url));
		
		

$publicationsf = $xml_info->addChild('posts');

foreach($obj->data as $key => $value) {
	if(property_exists($value, 'message')) {
		$publications = $publicationsf->addChild('messages');
		$publications->addChild('message', htmlspecialchars("$value->message"));
		if(property_exists($value, 'likes')) {
			$publications->addChild('likes', $value->likes->summary->total_count);
		} else {
			$publications->addChild('likes', 0);
		}
		if(property_exists($value, 'shares')) {
			$publications->addChild('shares', $value->shares->count);
		} else {
			$publications->addChild('shares', 0);
		}
		if(property_exists($value, 'comments')) {
			$publications->addChild('comments', $value->comments->summary->total_count);
		} else {
			$publications->addChild('comments', 0);
		}
		if(property_exists($value, 'id')) {
			$diyurl = explode("_", $value->id);
			$publications->addChild('url', "https://www.facebook.com/" . $diyurl[0] . "/posts/" . $diyurl[1]);
		} else {
			$publications->addChild('url', 0);
		}
		if(property_exists($value, 'created_time')) {
			$fbdate = strtotime($value->created_time);
			$mydate = date('d m Y, h:m', $fbdate);
			$mydate_ = explode(" ", $mydate);
			$meses =  array('', 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');
			$mydate_[1] = $meses[(int)$mydate_[1]];
			
			$publications->addChild('created_time', implode(' ', $mydate_));
		}
		if(property_exists($value, 'updated_time')) {
			$fbdate = strtotime($value->updated_time);
			$publications->addChild('updated_time', date('d m Y, h:m', $fbdate));
		}
	}
}

header('Content-type: text/xml');
echo $xml_info->asXML();
?>